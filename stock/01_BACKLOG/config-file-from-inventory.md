

# Créer un configuration à partir des données de l'inventory

par exemple pour mettre en place un reverse proxy devant des serveurs web

```jinja
{% for item in groups[‘web’] %}
 
server {{ hostvars[item][‘inventory_hostname’] }} {{ hostvars[item][‘ansible_default_ipv4’][‘address’] }}:80 check
 
{% endfor %}

```

* [https://www.opensourceforu.com/2019/03/devops-series-using-ansible-to-set-up-haproxy-as-a-load-balancer-for-nginx/](https://www.opensourceforu.com/2019/03/devops-series-using-ansible-to-set-up-haproxy-as-a-load-balancer-for-nginx/)
* https://stackoverflow.com/questions/45485461/ansible-generating-haproxy-config-only-for-several-servers-from-inventory