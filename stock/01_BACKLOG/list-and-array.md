

## utilisation d'un tableau multi valué

définir un les données
```yaml
keystodeploy:
  - name: Petra Meier
    sshkey: ssh-rsa AAAABCe233e423...
  - name: Horst Lehmann
    sshkey: ssh-rsa AAAABDdsds...
```

Et l'utiliser :

```yaml
- name: Deploy SSH-Keys to remote host
    authorized_key:
      user: functionaccount
      key: "{{ keystodeploy|map(attribute='sshkey')|join('\n') }}"
      exclusive: true
```

source : [https://www.andrehotzler.de/en/blog/technology/79-distribute-ssh-keys-elegantly-with-ansible.html](https://www.andrehotzler.de/en/blog/technology/79-distribute-ssh-keys-elegantly-with-ansible.html)