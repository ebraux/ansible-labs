

## utiliser de sclés ssh stockées dans un dossier

Créer le dossier "keys", et y copier les fichier de clé public "*.pub"

Utiliser
```bash
  - name: update SSH keys
    authorized_key:
      user: "{{ user_name }}"
      key: "{{ lookup('pipe','cat ../keys/*.pub') }}"
      state: present
      exclusive: yes

```