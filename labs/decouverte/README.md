# Découvrir Ansible

 ... et en profiter pour tester le modèle Pet vs Cattle.

Se déplacer dans le dossier du lab "decouverte"
```bash
cd decouverte
```

Et lancer le playbook : 
```bash
ansible-playbook cattle.yml
```

**Attention, l'objectif de ce lab est uniquement de lancer la commande et de voir le résultat, pas de comprendre le contenu des fichiers**


A la fin du lab, revenir dans le dossier principal des labs
```bash
cd ..
```




