# Gestion des variables dans Ansible



## Préparation

Se déplacer dans le dossier du lab "variables"
```bash
cd variables
```

---
## Variables de playbook

Définie dans la section `vars` d'un playbook :

```yaml
  # déclaration de la variable
  vars:
    package_name: fail2ban

  tasks:
      ...
      # utilisation de la variable
      name: "{{ package_name }}"
    ...
```

Lancement du playbook :
```bash
ansible-playbook  playbook-vars.yml
```


---
## Les Facts

Lors de la connexion à un Managed-Node, Ansible collecte des données sur l'environnement physique et logiciel.

Pour afficher les facts collectés, on peut utiliser le module 'setup'

```bash
ansible srv-01  -m ansible.builtin.setup
# "ansible_facts": {
#       ...
#       "ansible_architecture": "x86_64",
#       ...
#       "ansible_devices": {
#       ...
#       "ansible_distribution": "Ubuntu",
#       ...
#       "ansible_env": {
#       ...  
#       "ansible_processor": [
#       ...  
```

On peut utiliser des filtres pour limiter les informations retournées

Informations sur le type de Node, ici un container Docker :
```bash
ansible srv-01  -m ansible.builtin.setup  -a "filter=ansible_virtualization_type"
```
```bash
  "ansible_facts": {
      "ansible_virtualization_type": "docker",
      "discovered_interpreter_python": "/usr/bin/python3"
  },
```

Informations sur la distribution :
```bash
ansible srv-01  -m ansible.builtin.setup -a "filter=ansible_distribution*"
```
```bash
  "ansible_facts": {
      "ansible_distribution": "Ubuntu",
      "ansible_distribution_file_parsed": true,
      "ansible_distribution_file_path": "/etc/os-release",
      "ansible_distribution_file_variety": "Debian",
      "ansible_distribution_major_version": "20",
      "ansible_distribution_release": "focal",
      "ansible_distribution_version": "20.04",
      "discovered_interpreter_python": "/usr/bin/python3"
  },
```

Les facts peuvent être utilisés dans les playbooks, comme n'importe quelle autre variable. Il faut s'assurer d'avoir activé la variable `gather_facts: yes` (valeur par défaut)

Exemple pour afficher la distribution de chaque Managed-node : 
```bash
ansible-playbook  display-fact.yml
```

---

## Variables d'inventory

Les variables peuvent être définies dans l'inventory :

- Au niveau groupe
  - de façon globale (all)
  - de façon spécifiques à un groupe
- Au niveau des managed_node

La gestion des variables d'inventory est faite via un ensemble de fichiers structurés :

```bash
  ├── group_vars      : variables de groupes
  │   └── all.yml     :   - concerne tous les Managed-Nodes
  │   └── <name>.yml  :   - pour un groupe spécifique
  └── host_vars       : variables de Managed-Node
      └── <name>.yml  :   - pour un Managed-Node spécifique
```

Pour afficher les variables définies au niveau de l'inventory, utiliser la commande `ansible-inventory`

Pour afficher l'ensemble des variables
```bash
ansible-inventory --yaml --list
```

Pour afficher les variables définies pour un Managed-Node
```bash
ansible-inventory --yaml --host srv-01
```

## Exemple avec un inventory simple

Le dossier 'inventory_simple' contient :

- un fichier d'inventaire basique
- une arborescence déclarant
  - une variable globale : 'sample_group_var'
  - une variable spécifique à chaque Managed-Node : 'sample_dedicated_host_var'

Fichier d'inventory simple
```ini
srv-01
srv-02
```

La structure de variable correspondant :
```bash
group_vars
 `-- all.yml     :  group_var_commune
host_vars
 |-- srv-01.yml  :  host_var_specifique
 `-- srv-02.yml  :  host_var_specifique
```

Affichage des variables d'inventory :
```bash
ansible-inventory -i inventory_simple/hosts --yaml --list
```
``` yaml
all:
  children:
    ungrouped:
      hosts:
        srv-01:
          group_var_commune: set_in_group_ALL
          host_var_specifique: I_am_srv-01
        srv-02:
          group_var_commune: set_in_group_ALL
          host_var_specifique: I_am_srv-02
```
* on retrouve la variable 'group_var_commune' avec la même valeur sur tous les Managed-Nodes
* la variable 'host_var_specifique', spécifique à chaque Managed-Nodes

---
## Exemple avec un inventory plus complexe

Le dossier 'inventory_complexe' contient :

- un fichier d'inventaire comprenant
  - 2 groupes de serveurs, avec chacun 2 groupes enfants
  - 4 Managed-Nodes
  - certains Mananged-node appartiennent à plusieurs groupes
- une arborescence déclarant
  - une variable commune à chaque groupe : 'group_var_commune'
  - une variables spécifique à chaque groupe : 'group_var_commune_<NOM_DU_GROUPE>'
  - une variable commune à chaque Managed-Node : 'host_var_specifique'

Fichier d'inventory complexe
```ini
[prod:children]
frontend_prod
backend_prod

[rec:children]
frontend_rec
backend_rec

[frontend_prod]
srv-front-01
srv-front-02

[backend_prod]
srv-back-01

[frontend_rec]
srv-rec-01

[backend_rec]
srv-rec-01
```

La structure de variable correspondant :
```bash
group_vars
 |-- all.yml
 |-- backend_prod.yml
 |-- backend_rec.yml
 |-- frontend_prod.yml
 |-- frontend_rec.yml
 |-- prod.yml
 `-- rec.yml
host_vars
 |-- srv-back-01.yml
 |-- srv-front-01.yml
 |-- srv-front-02.yml
 `-- srv-rec-01.yml
```


Affichage des variables d'inventaire :
```bash
ansible-inventory -i inventory_complexe/hosts --yaml --list
```
``` yaml
all:
  children:
    prod:
      children:
        backend_prod:
          hosts:
            srv-back-01:
              group_var_commune: set_in_group_BACKEND-PROD
              group_var_commune_all: all
              group_var_specific_backend_prod: backend-prod
              group_var_specific_prod: prod
              host_var_specifique: I_am_srv-back-01
        frontend_prod:
          hosts:
            srv-front-01:
              group_var_commune: set_in_group_FRONTEND-PROD
              group_var_commune_all: all
              group_var_specific_frontend_prod: frontend-prod
              group_var_specific_prod: prod
              host_var_specifique: I_am_srv-front-01
            srv-front-02:
              group_var_commune: set_in_group_FRONTEND-PROD
              group_var_commune_all: all
              group_var_specific_frontend_prod: frontend-prod
              group_var_specific_prod: prod
              host_var_specifique: I_am_srv-front-02
    rec:
      children:
        backend_rec:
          hosts:
            srv-rec-01:
              group_var_commune: set_in_group_FRONTEND-REC
              group_var_commune_all: all
              group_var_specific_backend_rec: backend-rec
              group_var_specific_frontend_rec: frontend-rec
              group_var_specific_rec: rec
              host_var_specifique: I_am_srv-rec-01
        frontend_rec:
          hosts:
            srv-rec-01: {}
    ungrouped: {}
```

* La variable 'group_var_commune_all' est identique pour tous les noeuds.
* La variable 'group_var_commune' contient la valeur du groupe enfant auquel appartient le noeud. La valeur 'set_in_group_ALL' du groupe parent est écrasée.
* La variables 'group_var_specific<NOM_DU_GROUPE>' est spécifique à chaque groupe (all, parent, child)
* La variable 'host_var_specifique', est spécifique à chaque Managed-Nodes

Quelques mises en garde :

* Pour `srv-rec-01` qui était présent dans 2 groupes "frontend_rec" et "backend_rec", la valeur "group_var_commune" ne contient que "set_in_group_FRONTEND-REC". Une partie des données est donc perdue !! (la valeur 'set_in_group_BACKEND-REC').
* L'ordre de priorité est important !!
* Attention à la cohérence entre les variables et les groupes de l'inventaire



---
## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---
## Informations Complémentaires

Quelques liens :

* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#understanding-variable-precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#understanding-variable-precedence)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#vars-and-facts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#vars-and-facts)


Pour aller plus loin :

* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables)
* [https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html](https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html)
* [https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters](https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters)
