# Environnement pour les labs Ansible, basé sur Docker.


## Prérequis : Utilisation de Docker pour mettre à disposition l'environnement des labs

Docker permet de façon relativement simple de lancer plusieurs instances sur une machine, même peu puissante.

Vous n'aurez pas à utiliser Docker directement, aucune compétence Docker n'est nécessaire pour déployer l'environnement ou réaliser les labs.

 Il est toutefois nécessaire d'installer Docker et Docker-compose sur votre poste, ainsi que la commande "make". Un poste de travail Linux est recommandé (ou une VM)

* Documentation d'installation de Docker et Docker-compose pour Ubuntu 20.04 :
    * [docker-install.md](docker-install.md)
    * [docker-install-compose.md](docker-install-compose.md)
* Autres :
    * [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)
    * [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
    * [https://computerz.solutions/win10-installer-docker/](https://computerz.solutions/win10-installer-docker/)
    * [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)
  

> Rappel :
> Ansible n'est pas lié à Docker, et peut être installé sur tout type de machine.
> Un environnement identique de Labs pourrait être mis en place avec des Machines Virtuelles sur votre poste local, chez un cloud provider, sur des RasberryPy, ...


Un fichier "Makefile" est disponible pour lancer toutes les commandes nécessaires au Lab, mais vous pouvez également lancer les commandes Docker manuellement.

Des informations sur une utilisation directement avec Docker, sans Makefile, sont disponibles : [docker-infos.md](docker-infos.md).

---

## Mise en place de l'environnement et initialisation :

``` bash
cd docker
```

Création de l'environnement

- Commande make :
```bash
make create
```

> Ces opérations nécessitent une connexion internet, et peuvent prendre un peu de temps. Pour la suite des labs, le démarrage de l'environnement sera rapide, et pourra être effectué sans connexion à Internet, sauf pour les installations de packages.

Le dossier local courant est partagé entre la machine hôte (votre ordinateur) et la machine virtuelle de travail (Control-Node).


## Lancement de l'environnement

Les commandes suivantes sont à exécuter _à chaque démarrage de votre poste de travail_, dans le répertoire de travail dédié à cette formation, sur votre ordinateur.

Une fois l'initialisation des machines faite, vous allez pouvoir lancer les _machines virtuelles_

- Commande make :
```bash
make start
```

Pour tester que tout s'est bien déroulé dans les étapes *initialisation* et *lancement*, un test peut-être réalisé :

```bash
make test
```
Le test se connecte sur le Control-Node, lance la commande Ansible, et effectue un "ping" sur les Managed-Nodes



## Utilisation de l'environnement de TP

Vous avez maintenant 3 machines virtuelles fonctionnelles, préconfigurées pour les TPs. Vous pouvez vous connecter à la machine virtuelle de travail.

Récupérer un shell sur la machine de travail (control-Node).
```bash
make go
```
Cette commande lance un shell sur le Control-Node. Pour revenir à votre environnement, il faut sortir du shell, avec la commande `exit`.


## Suivi de l'environnement

### Gestion 

Start/stop des Labs, sans destruction, pour faire une pause :
```bash
make start
make stop
```

Suppression des ressources "actives" des Labs, mais pas des données :
```bash
make delete
```

Suppression de toutes les ressources des Labs, mais pas des données :
```bash
make clean
```

### Suivi/Test

Affichage des ressources du Lab :
```bash
make status
```

Affichage des logs liés au Lab : 
```bash
make logs
```

Test du bon fonctionnement du Lab
```bash
make test
```

