# Installation de docker-compose

## Installation

Vérifier la dernière version disponible dans la page des versions : [https://github.com/docker/compose/releases](https://github.com/docker/compose/releases)

Nous utilisons la version 1.27.4.

Téléchager docker-compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Rendre le fichier exécutable
```language
sudo chmod +x /usr/local/bin/docker-compose
```

Vérifier le bon fonctionnement :
```bash
docker-compose --version
  docker-compose version 1.27.4, build 40524192
```

## Validation 

Créer un fichier docker-compose.yml de test avec un service "hello" :
```yaml
version: '3'
services:
  hello:
    image: hello-world
```

Lancer le service "hello"
```bash
docker-compose up
  ...
 Hello from Docker!
 This message shows that your installation appears to be working correctly.
  ...
```

Vérifier le service
```bash
docker-compose ps
      Name       Command   State    Ports
  ---------------------------------------
  user_hello_1   /hello    Exit 0        
```

Supprimer le service
```bash
docker-compose down
  Removing user_hello_1 ... done
  Removing network user_default
```

Ménage
```bash
docker image rm hello-world
```

## Références

* [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-fr)
