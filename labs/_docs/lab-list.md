# Liste des labs

## Liste des labs Thématiques

- [Découverte](decouverte) - Découverte de l'environnement et lancement de Ansible
- [L'inventory](inventory) - Utilisation de l'inventory pour organiser ses tâches
- [Configuration et Commandes adhoc](adhoc) - Prise en main de Ansible : Apperçu de la configuration, et lancement de commandes AdHoc
- [Les playbooks](playbooks) - Utilisation de playbooks
- [Les variables](variables) - Utilisation des variables
- [Les templates](template) - Utilisation des templates Jinja2
- [Les roles](roles) - Utilisation de roles
- [Les boucle, conditions, ...][control] - boucle, conditions, ...


## Liste des labs Labs "Cas d'usage"

- [Initialiser un Managed Node](secure-connexion)
- [Déployer une application WEB](web-app-lamp)

