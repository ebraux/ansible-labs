# Informations sur l'utilisation de Docker

## Initialisation de l'environnement Docker

Initialisation
```bash
$ docker-compose -f docker/docker-compose.yml build
```
> Docker va récupérer les images de base, et builder les images utilisées dans les labs, et déployer l'environnement des labs. Ce qui nécessite une connexion internet, et peut prendre un peu de temps. Pour la suite des labs, le démarrage de l'environnement sera rapide, et pourra être effectué sans connexion à Internet, sauf pour les installations de packages.


### Exemple d'utilisation de Ansible

Ansible sera utilisé depuis la machine "Control Node"

Se positionner dans le dossier "ansible-labs", et lancer l'environnement. : 
```bash
$ docker-compose -f docker/docker-compose.yml up -d
```

Connexion à la machine "Control Node". 
```bash
$ docker exec -it ansible-lab-controler bash
```
> La connexion ne se fait pas par SSH, mais par le lancement d'un shell dans le container


Une fois dans le container, lancement de la commande ansible : 
```bash
root@controler:/labs$ ansible --version
ansible 2.9.6
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 3.8.5 (default, Jul 28 2020, 12:59:40) [GCC 9.3.0]
```


Vous retrouvez les labs qui on précedement été chargés dans le dossier de travail par défaut, "/labs".
```bash
root@controler:/labs$ ls -la
```

Pour vous dé-connecter de la machine "Control Node", sortir du shell :
```bash
root@controler:/labs$ exit
```


## Gestion de l'environnement des Labs

### Start/stop

Lancement de l'environnement
```bash
$ docker-compose -f docker/docker-compose.yml up -d
```

Arrêt de l'environnement
```bash
$ docker-compose -f docker/docker-compose.yml stop
```

Re-démarrage de l'environnement
```bash
$ docker-compose-f docker/docker-compose.yml  start
```

Suppression de l'environnement (sauf les donénes de labs) l'environnement
```bash
$ docker-compose-f docker/docker-compose.yml  down
```


### En cas de besoin 

Vérification des images après la commande `docker-compose -f docker/docker-compose.yml  build`:

```bash
$ docker images
REPOSITORY           TAG     IMAGE ID      CREATED         SIZE
ansible-labs-target  latest  44976dde9b68  23 seconds ago  269MB
ansible-labs-manager latest  8fac73fed906  24 seconds ago  416MB
ansible-labs-base    latest  5489fd439e6f  51 seconds ago  269MB
ubuntu               focal   f643c72bc252  3 weeks ago     72.9MB
```


Vérification que les 3 containers ont bien été lancés après la commande `docker-compose -f docker/docker-compose.yml up -d`:
``` bash
$ docker-compose ps
Name                  Command                       State  Ports 
---------------------------------------------------------------
ansible-lab-controler /bin/bash                      Up
ansible-lab-srv-01    /bin/sh -c service ssh sta ... Up    22/tcp
ansible-lab-srv-02    /bin/sh -c service ssh sta ... Up    22/tcp
environement_docker_base-image_1   /bin/bash         Exit 0
```

Pour vérifier l'accès aux container, on se connecte en exécutant la commande bash dans le container, et non via SSH  :
```bash
$ docker exec -it ansible-lab-controler bash
root@manager:/work#
exit
```

``` bash
$ docker exec -it ansible-lab-srv-01 bash

$ docker exec -it ansible-lab-srv-02 bash
```

Si besoin, pour lister les containers, et afficher les logs
``` bash
$ docker-compose -f docker/docker-compose.yml ps
$ docker-compose -f docker/docker-compose.yml logs
```


