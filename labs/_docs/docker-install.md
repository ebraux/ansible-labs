
# Installation de Docker

## Préparation

Ajouter la clé GPG du dépôt officiel de Docker au système :
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Ajoutez le dépôt Docker aux sources APT :
```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt-get update
```

Vérifier que le dépôt est bien celui de Docker, et non celui d'Ubuntu par défaut :
```bash
apt-cache policy docker-ce
```
> Dans "table de version", les "sources" des candidats à l'installation doivent provenir du dépôt Docker pour Ubuntu 20.04 (focal).

## Installation

Installer Docker :
```bash
sudo apt install -y docker-ce
```

Vérifier que Docker a bien démarré en mode daemon, le service doit avoir le status "active (running)" :
```bash
sudo systemctl status docker
```
Vérifier la version
```bash
docker --version
  Docker version xx.xx.xx, build xxxxxxxxxx
```

Autoriser l'utilisation de Docker sans "sudo" pour l'utilisateur courant :
```bash
sudo usermod -aG docker ${USER}
```

Lancer un nouveau shell, pour prendre en compte cette modification
```bash
sudo usermod -aG docker ${USER}
su - ${USER}
```
> Vous serez invité à saisir le mot de passe utilisateur pour continuer.

Tester le bon fonctionnement :
```bash
docker stats
```

## Validation

Lancer le container Hello World
```bash
docker run --rm hello-world
  ...
 Hello from Docker!
 This message shows that your installation appears to be working correctly.
  ...
```

Ménage
```bash
docker image rm hello-world
```

## Références

* [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-fr)
