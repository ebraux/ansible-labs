# Utilisation de Rôles

## Préparation

Se déplacer dans le dossier du lab "roles"
```bash
cd roles
```

---
## Configuration du Lab

Ansible gère les rôles dans le dossier défini par la variable `roles_path`, dont la valeur est par défaut `/etc/ansible/roles`.

Pour ce lab, cette variable est redéfinie pour stocker les rôles dans le dossier "./roles"

```bash
cat ansible.cfg
# ...
# roles_path     = ./roles
# ...
```

---
## Création d'un rôle basique : installation de 'cowsay'

Tâche à réaliser :

- Installation du package 'cowsay'
- Pas de variable, ni de valeur par défaut, ni de template


Rappel : structure de fichier de référence d'un rôle :

```bash
├── README.md     : description du role
├── defaults      : variables par défaut du rôle
│   └── main.yml
├── files         : contient des fichiers à déployer
├── handlers      : actions déclenchées par une notification
│   └── main.yml
├── meta          : metadonnées et notamment les dépendances
│   └── main.yml
├── tasks         : liste des tâches à exécuter
│   └── main.yml
├── templates     : des templates au format Jinja2
|   └── template.j2
└── vars          : autres variables pour le rôle
    └── main.yml
```

---
## Création du rôle

Création d'un rôle en utilisant la commande `ansible-galaxy`, qui permet de générer un "squelette" qui correspond aux standards d'Ansible:
``` bash
cd roles
ansible-galaxy init  cowsay
```

Observer l'arborescence créée :
``` bash
tree cowsay
```

Pour notre rôle, les ressources nécessaires se limitent donc à 1 tâche : installation du package `cowsay`.

Il faut donc modifier la tache par défaut du rôle :
```
cowsay
   `-- tasks
       `-- main.yml
```

Création du fichier de tache par défaut 
```bash
tee  ./cowsay/tasks/main.yml << EOF
---
# tasks file for cowsay
- name: Install cowsay
  ansible.builtin.apt:
    name: ['cowsay']
    state: present
EOF
```
Et observation du résultat : `cat cowsay/tasks/main.yml`

Retour à la racine de ce lab : `cd ..`

---
## Utilisation du rôle


Pour utiliser ce rôle, il faut l'appeler dans un playbook :
``` bash
cat playbook_my-cowsay.yml
```

```yaml
- hosts: all
  roles:
  - cowsay
```

Lancement du playbook
```bash
ansible-playbook playbook_my-cowsay.yml
```

Test
```bash
ansible all -m ansible.builtin.command -a 'cowsay hello'
```

---
## Installation de cowsay en utilisant ansible galaxy

Une recherche sur le portail Ansible Galaxy [https://galaxy.ansible.com/](https://galaxy.ansible.com/) renvoie plusieurs résultats, dont  [https://galaxy.ansible.com/jsecchiero/cowsay](https://galaxy.ansible.com/jsecchiero/cowsay).


Installation du rôle
```bash
ansible-galaxy install jsecchiero.cowsay
# Starting galaxy role install process
# - changing role jsecchiero.cowsay from master to unspecified
# - downloading role 'cowsay', owned by jsecchiero
# - downloading role from https://github.com/jsecchiero/ansible-cowsay/archive/master.tar.gz
# - extracting jsecchiero.cowsay to /labs/roles/roles/jsecchiero.cowsay
# - jsecchiero.cowsay (master) was installed successfully
```

Vérification
```bash
ls roles
# cowsay  jsecchiero.cowsay
```


Et appel du rôle `cat playbook_jsecchiero-cowsay.yml`
```yaml
- hosts: all
  roles:
  - jsecchiero.cowsay
```

Lancement
```bash
ansible-playbook playbook_jsecchiero-cowsay.yml
```

Si on regarde le rôle de plus près

- l'arborescence "standard" a été mise en place
- on retrouve
    - une tâche dans le fichier `tasks/main.yml`
    - la déclaration de la  valeur par défaut de la chaîne à afficher 'roar' à afficher dans `defaults/main.yml`
    - les informations sur le rôle dans `meta/main.yml`
    - un playbook de test dans `test`
    - et bien sûr un fichier `README.md`

On peut donc modifier le comportement de ce rôle en modifiant la variable 'roar' :
```yaml
  vars:
  - roar: 'Hello'
```

Affichage du playbook : `cat playbook_jsecchiero-cowsay-hello.yml`

Lancement
```bash
ansible-playbook playbook_jsecchiero-cowsay-hello.yml
```

---
## Utilisation d'un fichier de "requirement.yml"

La commande `ansible-galaxy` peut également être utilisée pour gérer les dépendances et versions des rôles.

Définition de la liste des rôles à installer, et leur version dans un fichier `requirements.yml` :
```yaml
---
- src: jsecchiero.cowsay
```

Installation
``` bash
ansible-galaxy install -r requirements.yml
```



## Etude d'un rôle un peu plus évolué

Installons un rôle pour fail2ban, installé précédemment : [https://galaxy.ansible.com/oefenweb/fail2ban](https://galaxy.ansible.com/oefenweb/fail2ban)

```bash
ansible-galaxy install oefenweb.fail2ban
```

- Consulter le fichier README.md, pour la liste des Options
- parcourir l'arborescence
- ...

---
## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---
## Informations Complémentaires

Quelques liens :

- Le portail Ansible Galaxy : [https://galaxy.ansible.com/](https://galaxy.ansible.com/)
- Le point d'entrée vers la documentation : [https://galaxy.ansible.com/docs/](https://galaxy.ansible.com/docs/)
- Rechercher du contenu dans ansible-galaxy : [https://galaxy.ansible.com/docs/finding/search.html](https://galaxy.ansible.com/docs/finding/search.html)



