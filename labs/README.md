# Lab d'introduction à Ansible


## Objectif

L'objectif de ces Labs est de découvrir Ansible d'une façon pratique.

Il ne s'agit pas de maîtriser l'utilisation d'Ansible, mais de comprendre comment Ansible fonctionne.


---
## Présentation des labs

Une présentation des labs est disponible :

- Version en ligne: [ansible-labs.html](ansible-labs.html)
- version PDF: [ansible-labs.pdf](ansible-labs.pdf)

---

## Récupération des labs

Les labs sont disponibles à l'adresse : [https://gitlab.com/ebraux/ansible-labs](https://gitlab.com/ebraux/ansible-labs)

Pour installer les labs sur votre poste :
```bash
git clone https://gitlab.com/ebraux/ansible-labs.git
```

----

## Environnement des Labs


### Architecture

Les labs proposés ont été conçus pour fonctionner dans un environnement composé d'un Control-Node, et de 2 Managed-Nodes.

Sur le Control-Node, sont installés :

- Ansible
- divers outils permettant d'interagir avec les Managed-Nodes (ping, curl ...)
- divers outils permettant de travailler sur les labs (nano, ...)

Sur les Managed-Node, seul openssh-server est installé pour permettre les connexions par SSH. Les Managed-Nodes seront configurés via Ansible à mesure du déroulement des Labs.



### Configuration SSH

Pour simplifier la prise en main, de Ansible une configuration SSH simplifiée, et non-secure est mise en place.

L'utilisateur "user" du Controle-Node peut se connecter en tant que "root" en utilisant SSH sur les Managed-Nodes :

- soit en utilisant un mot de passe,
- soit sans mot de passe, en utilisant sa clé publique.

L'utilisateur "root" du Control-Node peut également se connecter dans les mêmes conditions.


Le mot de passe à utiliser dans toutes les situations est : `unsecure`.


### Environnement d'exécution des labs


Pour faciliter le déroulement des Labs, et se concentrer sur Ansible, et non sur l’environnement technique, des environnements "pré-fabriqués" sont disponibles.

Ces environnements permettent de simplifier la mise en place des labs :

- récupération du code source des labs dans le dépôt git.
- déploiement et configuration du Control-Node et des 2 Managed-Nodes
- accès au Control-Node

Des scripts utilisant la commande Unix "make" ont étés développés, pour simplifier le déploiement de l'environnement :

- `make create` : création de l'environnement
- `make start`  : lancement des labs
- `make test`   : test du bon fonctionnement de l'environnement
- `make go`     : accès au Control-Node pour dérouler les labs
- `make stop`   : arrêt des labs
- `make delete` : suppression des ressources "actives" du Lab, mais pas des données
- `make clean`   : suppression des données créée par le lab, en dohosr du dépot.

Pour l'instant seul un environnement s'appuyant sur Docker est disponible : [Environnement Docker](_docs/docker-environnement.md)

