# Premiers pas avec Ansible, utilisation du mode AdHoc, et configuration


## Préparation

Se déplacer dans le dossier du lab "adhoc"
```bash
cd adhoc
```

## Première commande

La première commande va permettre de tester que les Managed-Nodes sont actifs et accessibles.

On utilise le module "ping", avec le mode Ad-Hoc

``` bash
# password = unsecure
ansible all -i srv-01,srv-02 -u user --ask-pass -m ansible.builtin.ping
#            |   |                |                  |
#            |   |                |                  └──>  Utilisation du module
#            |   |                |                        "ansible.builtin.ping"
#            |   |                └──>  connexion
#            |   |                      en tant que "user"
#            |   └──> création d une liste
#            |        de Managed-Nodes: "srv-01" et "srv-02"
#            └──>  "pattern", qui permet d'indiquer les
#                   Managed-Node à traiter
```

La commande est exécutée sur tous les Managed-Nodes :
```json
srv-01 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
srv-02 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```
> Remarque : dans la commande, le  `-m ping` fonctionne également à la place de `-m ansible.builtin.ping`

 
On peut également utiliser le module shell pour lancer des commandes système, comme afficher l'espace disque disponible avec `df`, ou afficher la mémoire disponible avec `free` :

```bash
# password = unsecure
ansible all -i srv-01,srv-02 -u user --ask-pass -m ansible.builtin.shell -a 'free -h'
```

Ce n'est pas très complexe, mais pas vraiment *Simple*, comme annoncé :

* la ligne de commande est un peu longue
* on ne fait rien de plus que ce qui peut être fait à la main directement

L'avantage tout de même, si on lance la commande sur plusieurs serveurs, le mot de passe n'a besoin d'être saisi qu'une seule fois.

## Utiliser un fichier d'inventory

La liste des Managed-Nodes (inventory) peut être gérée dans un fichier.

```bash
tee  ./hosts << EOF
srv-01
srv-02
EOF
```

Vérifier le fichier hosts
```bash
cat ./hosts
# srv-01
# srv-02
```

Pour utiliser un fichier d'inventory, utiliser la syntaxe :
```bash
# password = unsecure
ansible all -i hosts -u user --ask-pass -m ansible.builtin.ping 
```

## Ne plus saisir de mot de passe

Saisir le mot de passe à chaque commande, n'est pas très ergonomique.

Par défaut, Ansible se connecte via SSH, et utilise l'utilisateur courant. Si une connexion sans mot de passe a été configurée, Ansible sait l'utiliser.

L'environnement de Labs est configuré pour que l'utilisateur "user" du Control-Node puisse se connecter en tant que "user" sans mot de passe sur les Managed-Node, grâce au mécanisme de SSH Keys. Ce mécanisme est détaillé dans le Lab sur la "Connexion".

Testons l'environnement :
```bash
ssh srv-01 'free -h'
ssh srv-02 'free -h'
```

La syntaxe de la commande ansible peut donc être simplifiée :
```bash
ansible all -i hosts -m ansible.builtin.ping
ansible all -i hosts -m ansible.builtin.shell -a 'free -h'
```

## Utiliser un fichier de configuration pour Ansible

Le comportement de Ansible peut être configuré, grâce à un fichier de configuration "ansible.cfg" à placer dans le dossier dans lequel la commande est executée.

Dans notre cas, nous souhaitons configurer que le fichier d'inventory à utiliser est le fichier 'hosts'.

```bash
tee ./ansible.cfg << EOF
[defaults]
inventory      = ./hosts
ansible_python_interpreter = /usr/bin/python3
EOF
```

Vérifier le fichier ansible.cfg
```bash
cat ./ansible.cfg
```
> Un fichier de configuration complet et documenté est disponible dans /etc/ansible/ansible.cfg

La commande se résume finalement à :
```bash
ansible all -m ansible.builtin.ping
ansible all -m ansible.builtin.shell -a 'free -h'
```

Là, on peut dire que c'est simple.


## Lancer des commandes avec élévation des privilèges


Certaines commandes nécessitent d'être exécutées par un autre utilisateur que celui utilisé pour se connecter au Managed-Node.

Par exemple, l'installation d'un package doit être exécutée en tant qu'utilisateur "root". Ansible permet les changements d'utilisateur. Par défaut avec la commande "sudo".

```bash
ansible all -m ansible.builtin.apt -a 'name=sl state=latest'
```
Cette commande retourne une erreur, car "user" n'a pas le droit d'installer des packages
```bash
...
E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)
...
```

L'option "--become" permet de changer d'utilisateur. Par défaut on devient l'utilisateur "root".
```bash
ansible all -m ansible.builtin.apt -a 'name=sl state=latest' --become
```


## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---
## Informations Complémentaires

Tous les modules peuvent être utilisés avec le mode ad-hoc, mais il est préférable dans la plupart des cas d'utiliser des playbooks, même très simples.

Ansible propose un module "raw", qui peut-être utilisé pour installer les packages nécessaires pour Ansible sur le managed node
```bash
ansible all -m ansible.builtin.raw -a 'apt install -y python-simplejson' --become
```

Ansible propose le module "setup", qui permet de collecter des informations sur les Managed-Nodes, grâce au mécanisme de "Facts". Ce mécanisme est détaillé dans le Lab sur les "Variables".
```bash
ansible all  -m ansible.builtin.setup  -a "filter=ansible_distribution*"
```
 
Quelques liens :

Le mode AdHoc : [https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)

Le fichier inventory : [https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

Les modules :

* ping : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html)
* shell : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html)
* setup : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html)
* liste des modules : [https://docs.ansible.com/ansible/latest/collections/index_module.html](https://docs.ansible.com/ansible/latest/collections/index_module.html)

