
# Utilisation de playbooks

## Préparation

Se déplacer dans le dossier du lab "playbooks"
```bash
cd playbooks
```

---
##  Lancement d'un playbook qui ne fait rien

```bash
ansible-playbook  no-action.yml
```

Contenu du playbook :
```yaml
- hosts: all
```

Mais qui nous apprend tout de même des choses ...
```bash
PLAY [No action] ***************************************

TASK [Gathering Facts]  ********************************
ok: [srv-02]
ok: [srv-01]

PLAY RECAP *********************************************
srv-01  : ok=1 changed=0  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0
srv-02  : ok=1 changed=0  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0
```

On retrouve dans cette sortie :

* Le filtre sur les Managed-Node à traiter, indiqué dans "hosts"  : `PLAY [all]`
* La liste des Managed-Nodes concernés "srv-01" et "srv-02"
* Les actions menées, et leur résultat

Même si aucune tâche n'est présente dans le playbook, on obtient `ok=1`, ce qui indique qu'une tâche est effectuée. C'est une tâche "système" de collecte des données sur les Managed-Nodes (Gathering Facts), qui est effectuée par défaut. Pour la désactiver, il faut fixer la variable 'gather_facts' à 'no'.

```yaml
- hosts: all
  gather_facts: no
```
```bash
ansible-playbook  really-no-action.yml
```
```bash
PLAY [all] *************************
PLAY RECAP *************************
```

> La collecte des données sur les Managed-Node prend du temps et des ressources, et est inutile si on n'utilise pas ces données. La partie "Gathering Facts" est abordée dans le Lab sur les variables.

---
## Execution d'une tache

Exemple pour exécuter une tache qui affiche le nom de l'utilisateur qui execute les opérations demandées par Ansible :

- Utilisation de la commande système `whoamI`, en utilisant le module `ansible.builtin.command`
- Affichage du résultat, avec le module  `ansible.builtin.debug`

``` yaml
  tasks:
    - name: Run whoami command  # noqa no-changed-when
      ansible.builtin.command: whoami
      register: whoamicommand

    - name: Display whoami command result
      ansible.builtin.debug:
        var: whoamicommand.stdout_lines
```

Lancement du playbook
```bash
ansible-playbook  whoami.yml
```

Résultat
```bash
    ...
    "whoamicommand.stdout_lines": [
        "user"
    ]
    ...
```

Par défaut l'utilisateur qui exécute le playbook est celui utilisé pour la connexion.


## Élévation des privilèges

Pour exécuter une tâche en tant que root, il faut utiliser la variable "become". Si on fixe `become: true`, on obtient :

```bash
ansible-playbook  whoami-become.yml
```

```bash
    ...
    "whoamicommand.stdout_lines": [
        "root"
    ]
    ...
```

> La variable *become* peut être utilisée au niveau d'une tâche, ou de l'ensemble du playbook.

> Par défaut, "become" change l'utilisateur en "root", mais un autre utilisateur peut être spécifié en utilisant la variable "become_user"



## Lancement d'un playbook qui installe un package

Installation du package "fail2ban", en utilisant le module apt, ce qui donne la syntaxe YAML suivante :
```yaml
  - name: Installation d'un package avec le module apt
      ansible.builtin.apt:
      name: fail2ban
      state: present
```

Lancement du playbook :
```bash
ansible-playbook  apt.yml
```

Fail2ban n'était pas installé sur les Managed-Nodes, on a donc en résultat des opérations effectuées : `ok=1 changed=1`

Si on relance cette commande, comme fail2ban est déjà présent, on obtient : `ok=1 changed=0`. C'est l'idempotence, les opérations ne sont réalisées que si c'est nécessaire.


## Utilisation de variables

Des variables peuvent être utilisées pour améliorer la lisibilité et la ré-utilisabilité des playbooks, ce qui donne la syntaxe YAML suivante :
```yaml
  # déclaration de la variable
  vars:
    package_name: fail2ban

  tasks:
      ...
      # utilisation de la variable
      name: "{{ package_name }}"
    ...
```

Lancement du playbook :
```bash
ansible-playbook  vars.yml
```

## Affichage / Debug

La commande `free` utilisée dans le lab sur le mode Ad-hoc permettait d'afficher l'espace disponible sur les Managed-Nodes :
```bash
ansible all  -m ansible.builtin.command -a 'free -h'
```

Si on utilise un playbook pour lancer cette commande, on n'obtient aucune information en sortie. Ce n'est pas le but d'un playbook.
```bash
ansible-playbook free.yml
```

Toutefois, pour afficher le résultat de la sortie standard d'une commande, il est possible d'utiliser le module "debug":

```bash
ansible-playbook free-output.yml
```


## Vérification de la syntaxe

La syntaxe YAML du playbook peut être testée avant le lancement :
```bash
ansible-playbook --syntax-check error.txt
```
> Remarque : le nom du fichier est "error.txt" et nom "error.yaml" car tous les fichiers ".yaml" des labs sont testés automatiquement, et qu'un fichier ".yaml" ne peut pas contenir d'erreur dans ce lab.

```bash
ERROR! We were unable to read either as JSON nor YAML, these are the errors we got from each:
JSON: Expecting value: line 1 column 1 (char 0)

Syntax Error while loading YAML.
  did not find expected '-' indicator

The error appears to be in '/labs/playbooks/error.yml': line 12, column 5, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

   - name: Installation avec le module apt
    ansible.builtin.apt:
    ^ here
```

Message assez habituel, il faut bien le dire, lié à un problème d'indentation :
```bash
diff vars.yml error.txt
# 12c12
# <       ansible.builtin.apt:
# ---
# >      ansible.builtin.apt:
```

---
## Simulation du lancement d'un playbook

Il est possible de simuler le lancement du playbook, aucun changement ne sera apporté :
```bash
ansible-playbook --check vars.yml
```

---
## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---

## Informations Complémentaires

Quelques liens :

* [https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)
* [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html)





