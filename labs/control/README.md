

# Utilisation des Structures de contrôle


## Préparation

Se déplacer dans le dossier du lab "control"
```bash
cd control
```

---
## Boucles : loop

Permet d'appliquer une tache à plusieurs éléments.

- La liste des éléments est définie dans une liste
- La tache est appliquée à chaque élément de la liste 
- La variable `item` fait reference à l'élément de la liste en cours 

Exemple : installer plusieurs packages

``` yaml
    - name: Install several package
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
      loop:
        - fail2ban
        - rsync
```

```bash
ansible-playbook loop.yml
```

La liste pourrait être définie en tant que variable. Dans ce cas, la syntaxe serait par exemple :
``` yaml
  vars:
    list_of_packages:
        - fail2ban
        - rsync

  tasks:
    - name: Install several package
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
      loop: "{{ list_of_packages }}"
```

```bash
ansible-playbook loop-variable.yml 
```


La liste peut contenir des dictionnaires. Dans ce cas le nom du champs est ajouté à `item`, séparé par un point :

``` yaml
  tasks:
    - name: Add several package
      ansible.builtin.apt:
        name: "{{ item.name  + '=' + item.version }}"
        state: present
      loop:
        - { name: 'fail2ban', version: '0.11.2-6' }
        - { name: 'rsync',    version: '3.2.7-0ubuntu0.22.04.2' }
```
> Pour obtenir les versions disponibles d'un package : `apt-cache policy <package name>`

```bash
ansible-playbook loop-dictionnary.yml
```

* [https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_loops.html](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_loops.html)


---
## Conditional : When

exemple : Tester la distribution d'un Managed-Node

On utilise une condition sur le "fact" "os_family" :
``` yaml
    - name: Test if Debian family
      ansible.builtin.debug:
        msg: ' It is a Debian family box'
      when: ansible_facts['os_family'] == "Debian"
```      

```bash
ansible-playbook when.yml
```

Dans l'execution du playbook, les tache ne correspondnat à pas à l'OS ne sont pas executées : 
``` bash
PLAY [Playbook de demo de when] ******************************************************************************

TASK [Gathering Facts] ******************************************************************************
ok: [srv-02]
ok: [srv-01]

TASK [Test if Debian family] ******************************************************************************
ok: [srv-01] => {
    "msg": " It is a Debian family box"
}
ok: [srv-02] => {
    "msg": " It is a Debian family box"
}

TASK [Test if RedHat family] *******************************************************************************
skipping: [srv-01]
skipping: [srv-02]

PLAY RECAP ********************************************************************
srv-01 : ok=2  changed=0  unreachable=0  failed=0  skipped=1  rescued=0  ignored=0   
srv-02 : ok=2  changed=0  unreachable=0  failed=0  skipped=1  rescued=0  ignored=0
```


* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)


---
## Block 

Cas d'usage : regrouper un ensemble de taches avec une condition "when" sur le type d'OS, pour éviter de gérer la condition sur chaque tache

Exemple :
``` yaml
   - name: Debian/Ubuntu | Install Mariadb dependencies on Debian/Ubuntu systems
      block:
        
        # Ensemble de taches     
        ...
        - name: Debian/Ubuntu | Install Mariadb and dependencies on Debian/Ubuntu systems
        ...

      when: ansible_os_family == "Debian"    
```

``` bash
ansible-playbook block-family.yml
```

- [https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_blocks.html#grouping-tasks-with-blocks](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_blocks.html#grouping-tasks-with-blocks)


Cas d'usage : détecter et gérer une exception. 

- Définir un ensemble de taches
- Définir une action en cas d'erreur sur les taches avec le mot clé `rescue`
- Éventuellement définir une action quie s'execute tout le temps, avec le mot clé `always`

``` yaml
  tasks:
    - name: Handle the error
      block:
        
        # Ensemble de taches 
        ...
        
      rescue:
        # tache a executer en cas d'erreur
```        

Exemple : 

- Utilisation du  module "ansible.builtin.debug" pour afficher des messages
- Utilisation du module "ansible.builtin.command" le la commande `/bin/false` pour générer une erreur :


```bash
ansible-playbook block-error.yml
```

- [https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_blocks.html#handling-errors-with-blocks](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_blocks.html#handling-errors-with-blocks)



---
## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

