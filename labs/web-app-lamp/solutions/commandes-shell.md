
# Commandes shell pour installation Wordpress

## Installation de l'infrastructure

1. Mise à jour
```language
sudo apt update
```
2. Installer Mysql
```bash
# Installation
sudo apt install mysql-server
# Démarrage du service
sudo service mysql start
# Vérification
sudo service mysql status
```   
3. Installer Apache
```bash
# Installation
sudo apt -y install apache2
# Démarrage du service
sudo service apache2 start
# Vérification
sudo service apache2 start
```
4. Installer PHP et le module PHP pour Apache et mysql
```bash
# Installation
sudo apt install -y php libapache2-mod-php php-mysql
# Test de PHP
php -version
# ReDémarrage du service Apache
sudo service apache2 start
```

Test : 

```bash
#sudo apt -y install curl
#curl localhost

nano /var/www/your_domain/index.html
h1>It works!</h1>

<p>This is the landing page of <strong>your_domain</strong>.</p>

sudo nano /var/www/html/info.php
<?php
phpinfo();
?>

```
---> n'interprete pas le php ...


Source : 
* [https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-fr)


## Installation de l'infrastructure

Connexion à Mysql
```bash
sudo mysql
```

Création des ressources
```sql
CREATE DATABASE wpdb;
CREATE USER 'wpdbuser'@'%' IDENTIFIED WITH mysql_native_password BY 'wpdbpass';
GRANT ALL ON wpdb.* TO 'wpdbuser'@'%';
exit;
```

Test
```bash
mysql -u wpdbuser -pwpdbpass -D wpdb
```

```sql
SHOW DATABASES;
exit;
```


Source : 
* [https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-fr)


## Installation de Worpress

Télécharger WP-cli :  
```bash
# Instalation
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
# Test
wp --info

```

```bash
cd /var/www/html
sudo mkdir wp
sudo chown user.user wp
cd wp
# recupération de Wordpress
wp-cli core download --locale=fr_FR --version=latest
# generation du fichier de configuration
wp-cli config create --dbname=wpdb --dbuser=wpdbuser --dbpass=wpdbpass --dbhost=localhost --locale=fr_FR
# Installation de Worpress
wp-cli core install --url='localhost/wp' --title='demo ansible' --admin_user='admin' --admin_password='password' --admin_email=admin@demo-ansible.fr --skip-email

```
