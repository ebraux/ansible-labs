# Objectifs

1. Créer un rôle pour déployer Wordpress sur notre plateforme LAMP.
1. Utiliser Ansible Vault pour stocker des informations sensibles.


# Les `group_vars`

Pour mettre en place **Ansible Vault**, il est possible d'utiliser la notion de `group_vars` pour définir des variables spécifiques à un groupe d'hôtes. Ces variables vont venir surcharger celles définies au niveau du rôle.

Le calcul des priorités sur les valeurs des variables est défini sur le page suivante : https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#ansible-variable-precedence

```
~/ansible
   |
   \
   group_vars
     |
     \_ [group_name]
         |
         \_ main.yml   # fichier YAML en clair pour le déploiement
                       #   avec des données non confidentiels
         \_ vault.yml  # fichier YAML chiffré avec Ansible Vault
                       #   contenant des secrets pour le déploiement
```

# Sections à modifier du fichier `wp-config.php`

```php
<?php
...
// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'votre_nom_de_bdd');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'votre_utilisateur_de_bdd');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'votre_mdp_de_bdd');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');
```

et

```php
<?php
...
$table_prefix = 'wp_';
```

# infos pratiques
## Structure d'une archive Wordpress
_(limité à 1 niveau)_

```
wordpress
├── index.php
├── license.txt
├── readme.html
├── wp-activate.php
├── wp-admin/
├── wp-blog-header.php
├── wp-comments-post.php
├── wp-config-sample.php
├── wp-content/
├── wp-cron.php
├── wp-includes/
├── wp-links-opml.php
├── wp-load.php
├── wp-login.php
├── wp-mail.php
├── wp-settings.php
├── wp-signup.php
├── wp-trackback.php
└── xmlrpc.php
```
## Utilisation du module `stat` et de `register` pour un traitement conditionnel
```yaml
- name: Tester si le site est deja deploye
  stat:
    path: ...
  register: config

- name: Telecharge et decompresse l'archive de Wordpress
  unarchive:
    remote_src: true
    src: ...
    dest: ...
    extra_opts:
      - '--strip-components=1'
  when: not config.stat.exists
```

# Liste des tâches à réaliser

Pour rendre le rôle facilement réutilisable, nous allons définir des variables au format **jinja2**, pour l'utilisation du rôle, qui seront valuées au niveau `group_vars` lors de l'exécution du playbook.

!!! info
    Pour plus d'information sur l'utilisation du moteur de template JinJa2: https://jinja.palletsprojects.com/en/2.10.x/

| vars role (jinja) | valeurs par défaut |
| ----------------- | ------------------ |
| wp_dbname         | wordpress_db       |
| wp_dbuser         | wordpress_user     |
| wp_dbpass         | wordpress_password |
| wp_dbprefix       | wp_                |
| wp_dbhost         | sydre-db-XX        |
| wp_www_path       | /var/www/html      |

!!! warning
    pensez à remplacer le XX de la valeur par défaut de `wp_dbhost` par votre numéro.

- Créer un rôle `sydre.wp` qui réalisera :
    - tester l'existence du fichier `wp-config.php` qui indiquerait qu'un déploiement à déjà été réalisé (module `stat` et attribut `register` de la tâche; voir `ansible-doc stat`)
    - les tâches suivantes ne devront être réalisé qu'en cas de non-existence préalable du fichier `wp-config.php` (utilisation de l'attribut `when` et de la variable `register` de l'étape précédente):
        - le téléchargement de l'archive wordpress sur le site officiel (https://fr.wordpress.org/latest-fr_FR.tar.gz) et la décompression de cette archive dans le dossier `/var/www/html` de la machine `sydre-web-XX` (module `unarchive` avec `remote_src: yes`)
        - la recopie du fichier de configuration d'exemple (`wp-config-sample.php` en `wp-config.php` avec le module `copy`)
        - la configuration, avec le module `lineinfile`, des éléments de connexion MySQL dans le fichier `wp-config.php` de l'installation Wordpress
            - chercher le pattern regexp `^define.'DB_NAME', .*$` et substituer la ligne correspondante avec `define('DB_NAME', '{{ wp_dbname }}');`
            - chercher le pattern regexp `^define.'DB_USER', .*$` et substituer la ligne correspondante avec `define('DB_USER', '{{ wp_dbuser }}');`
            - chercher le pattern regexp `^define.'DB_PASSWORD', .*$` et substituer la ligne correspondante avec `define('DB_PASSWORD', '{{ wp_dbpass }}');`
            - chercher le pattern regexp `^define.'DB_HOST', .*$` et substituer la ligne correspondante avec `define('DB_HOST', '{{ wp_dbhost }}');`
            - chercher le pattern regexp `table_prefix =.*$` et substituer la ligne correspondante avec `$table_prefix = '{{ wp_dbprefix }}';`
    - définir les variables correspondantes dans `~/ansible/group_vars/all/vault.yml` tel que défini dans le tableau précédent
    - chiffrer le fichier `~/ansible/group_vars/all/vault.yml` avec **ansible-vault** : `ansible-vault encrypt --ask-vault-pass ~/ansible/group_vars/all/vault.yml`
    - visualiser le contenu du fichier `~/ansible/group_vars/all/vault.yml`, vous verrez qu'il n'est plus possible d'accéder aux valeurs, sauf à lancer un déchiffrement
- Se rendre sur le site WordPress à l'URL http://sydre-web-XX
    - Configurer le compte administrateur et l'URL et titre du site

!!! warning "Dépendance du module unarchive"
    le module `unarchive` nécessite que la commande `unzip` soit disponible sur le machine cible pour décompresse une archive .zip, il vous faudra prévoir son installation **avant** la première tâche utilisant le module `unarchive`.

!!! info
    Les pointeurs web de documentation nécessaires :

    - https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html
    - https://docs.ansible.com/ansible/latest/modules/unarchive_module.html
