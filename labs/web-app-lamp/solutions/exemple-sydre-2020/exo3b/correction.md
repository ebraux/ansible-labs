# Rôle `sydre.wp`

```bash
git clone --depth 1 https://gitlab.com/sydre-ansible-2020/ansible-role-sydre-wp.git ~/ansible/roles/sydre.wp
```

# group_vars/all/vault.yml (avant chiffrement)
```yaml
---
wp_dbname: 'wordpress_db'
wp_dbuser: 'wordpress_user'
wp_dbpass: 'wordpress_password'
wp_dbprefix: 'wp_'
wp_dbhost: 'sydre-db-01'
wp_www_path: '/var/www/html'
```

# Playbook `infra.yml`

```yaml
---
- name: Install Apache PHP
  hosts: webservers
  become: true
  roles:
    - sydre.apache

- name: Install MariaDB
  hosts: dbservers
  become: true

  vars:
    mariadb_bind_address: '0.0.0.0'

    mariadb_database:
      - name: '{{ wp_dbname }}'
        state: present

    mariadb_user:
      - name: '{{ wp_dbuser }}'
        host: '%'
        password: "{{ wp_dbpass }}"
        priv: '{{ wp_dbname }}.*:ALL'
        encrypted: false

  pre_tasks:
    - name: Install dependencies
      package:
        name: python-pymysql
        state: present

  roles:
    - diodonfrost.mariadb

- name: Install wordpress
  hosts: webservers
  become: true
  roles:
    - sydre.wp

```
