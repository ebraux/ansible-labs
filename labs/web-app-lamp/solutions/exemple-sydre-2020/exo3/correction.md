# Rôle `sydre.wp_cli`

```bash
git clone --depth 1 https://gitlab.com/sydre-ansible-2020/ansible-role-sydre-wp-cli.git ~/ansible/roles/sydre.wp_cli
```

# group_vars/all/main.yml
```yaml
---
wp_cli_locale: 'fr_FR'
wp_cli_version: 'latest'
wp_cli_title: 'Site web de XX'
wp_cli_url: '{{ ansible_fqdn }}'
```

# group_vars/all/vault.yml (avant chiffrement)
```yaml
---
wp_cli_dbname: 'wordpress_db'
wp_cli_dbuser: 'wordpress_user'
wp_cli_dbpass: 'wordpress_password'
wp_cli_dbprefix: 'wp_'
wp_cli_dbhost: 'sydre-db-01'
wp_cli_www_path: '/var/www/html'
wp_cli_admin_user: 'adminXX'
wp_cli_admin_password: 'passXX'
wp_cli_admin_email: 'adminXX@example.com'
```

# Playbook `infra.yml`

```yaml
---
- name: Install Apache PHP
  hosts: webservers
  become: true
  roles:
    - sydre.apache

- name: Install MariaDB
  hosts: dbservers
  become: true

  vars:
    mariadb_bind_address: '0.0.0.0'

    mariadb_database:
      - name: '{{ wp_cli_dbname }}'
        state: present

    mariadb_user:
      - name: '{{ wp_cli_dbuser }}'
        host: '%'
        password: "{{ wp_cli_dbpass }}"
        priv: '{{ wp_cli_dbname }}.*:ALL'
        encrypted: false

  pre_tasks:
    - name: Install dependencies
      package:
        name: python-pymysql
        state: present

  roles:
    - diodonfrost.mariadb

- name: Install wordpress
  hosts: webservers
  become: true
  roles:
    - sydre.wp_cli

```
