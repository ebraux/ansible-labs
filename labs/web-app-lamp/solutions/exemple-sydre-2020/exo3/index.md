# Objectifs

1. Créer un rôle pour déployer Wordpress sur notre plateforme LAMP.
1. Utiliser Ansible Vault pour stocker des informations sensibles.


# Les `group_vars`

Pour mettre en place **Ansible Vault**, il est possible d'utiliser la notion de `group_vars` pour définir des variables spécifiques à un groupe d'hôtes. Ces variables vont venir surcharger celles définies au niveau du rôle.

Le calcul des priorités sur les valeurs des variables est défini sur le page suivante : https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#ansible-variable-precedence

```
~/ansible
   |
   \
   group_vars
     |
     \_ [group_name]
         |
         \_ main.yml   # fichier YAML en clair pour le déploiement
                       #   avec des données non confidentiels
         \_ vault.yml  # fichier YAML chiffré avec Ansible Vault
                       #   contenant des secrets pour le déploiement
```



Pour rendre le rôle facilement réutilisable, nous allons substituer les différents arguments nécessaires à l'utilisation de wp-cli par des variables au format **jinja2** qui seront valuées au niveau `group_vars` lors de l'exécution du playbook.

!!! info
    Pour plus d'information sur l'utilisation du moteur de template JinJa2: https://jinja.palletsprojects.com/en/2.10.x/


| arg. binaire wp | vars role (jinja)     | valeurs par défaut  | fichier .yml group_vars |
| --------------- | --------------------- | ------------------- | ----------------------- |
| locale          | wp_cli_locale         | fr_FR               | main                    |
| version         | wp_cli_version        | latest              | main                    |
| dbname          | wp_cli_dbname         | wordpress_db        | vault                   |
| dbuser          | wp_cli_dbuser         | wordpress_user      | vault                   |
| dbpass          | wp_cli_dbpass         | wordpress_password  | vault                   |
| dbprefix        | wp_cli_dbprefix       | wp_                 | vault                   |
| dbhost          | wp_cli_dbhost         | sydre-db-XX         | vault                   |
| path            | wp_cli_www_path       | /var/www/html       | vault                   |
| url             | wp_cli_url            | sydre-web-XX        | main                    |
| title           | wp_cli_title          | Site web de XX      | main                    |
| admin_user      | wp_cli_admin_user     | adminXX             | vault                   |
| admin_password  | wp_cli_admin_password | passXX              | vault                   |
| admin_email     | wp_cli_admin_email    | adminXX@example.com | vault                   |

Deux variables supplémentaires sont introduites dans le rôle afin d'indiquer où installer l'outil `wp` :

- `wp_cli_path` (valeur par défaut `/usr/local/bin`)
- `wp_cli_bin` (valeur par défaut `{{ wp_cli_path + '/wp' }}` )



# Liste des tâches à réaliser

- télécharger / décompresser l'outil WP-cli sous le nom `wp`
    - module: `get_url`
    - url: https://raw.github.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    - destination:`{{ wp_cli_bin }}`
    - mode: `0755`

- installer Wordpress avec WP-cli:
    1. récupérer Wordpress
        - module: `command`
        - voir la sous-commande `core download`, attributs `path`, `locale` et `version`
        - et uniquement si `{{ wp_cli_www_path + '/wp-settings.php' }}` n'existe pas
    2. configuration de l'accès DB
        - module: `command`
        - voir la sous-commande `config create`, attributs `path`, `dbname`, `dbuser`, `dbpass`, `dbhost`, `dbprefix`, `skip-check`
        - et uniquement si `{{ wp_cli_www_path + '/wp-config.php' }}` n'existe pas
    3. configuration du compte admin Wordpress par défaut
        - module: `command`
        - voir la sous-commande `core install`, attributs `path`, `url`, `title`, `admin_user`, `admin_password`, `admin_email`, `skip-email`

# Implémenter du rôle `sydre.wp_cli`

- créer la structure du rôle avec `ansible-galaxy` (voir exercice 2)
- implémenter les tâches décrites précédemment
- définir les variables `wp_cli_*` citées ci-dessus et leurs valeurs par défaut dans `~/ansible/roles/sydre.wp_cli/defaults/main.yml`
- ajouter le play correspondant dans le playbook `infra.yml` pour le groupe `webservers`
- définir les variables correspondantes dans `~/ansible/group_vars/all/{main,vault}.yml` tel que défini dans le tableau précédent

    !!! info "fichier ~/ansible/group_vars/all/main.yml"
        ```yaml
        wp_cli_version: latest
        ```

    !!! info "fichier ~/ansible/group_vars/all/vault.yml"
        ```yaml
        wp_cli_admin_password: adminXX
        ```

- chiffrer le fichier `~/ansible/group_vars/all/vault.yml` avec **ansible-vault** : `ansible-vault encrypt --ask-vault-pass ~/ansible/group_vars/all/vault.yml`
- visualiser le contenu du fichier `~/ansible/group_vars/all/vault.yml`, vous verrez qu'il n'est plus possible d'accéder aux valeurs, sauf à lancer un déchiffrement
- exécuter le playbook pour installer Wordpress avec l'option `--ask-vault-pass` afin de déchiffrer le fichier `vault.yml` pour la durée de l'exécution
- tester le bon fonctionnement du Wordpress en consultant l'URL: http://sydre-web-XX
