# Déploiement d'un environnement pour une application WEB

## Principe

Déploiement de l'application de Blog "Wordpress" : [https://fr.wordpress.org/](https://fr.wordpress.org/.)

Wordpress est une application écrite en PHP, qui utilise une base de données.

La configuration la plus répendue est l'utilisation du serveur WEB Apache, configuré pour utiliser PHP, et la base de données MySQL : LAMP (Linux Apache Mysql PHP)

Ce Lab se déroule en plusieurs phases

La première phase a pour objectif d'écrire des playbooks simples permettant de réaliser toutes les étapes de l'installation.

Les autres phases permettent ensuite de s'appuyer sur les options plus avancées de Ansible (variable, rôles, ...)


## Informations techniques sur les installations

### Infrastructure LAMP

Les packages LAMP pour Ubuntu sont : mysql-server, apache2, mysql, php, libapache2-mod-php, php-mysql


### Wordpress

L'installation de Wordpress peut être automatisée en utilisant l'outil en ligne de commande "WP-cli": [https://wp-cli.org](https://wp-cli.org)

"WP-cli" peut être téléchargé à l'adresse
[https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar](https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar)

Les commandes utiles pour ce lab sont :

- Téléchargement de  Wordpress
```bash
wp-cli core download --locale=fr_FR --version=latest --path=/var/www/html/wp
```
- configuration de Wordpress (accès base de données, ...)
```bash
wp-cli config create --dbname=wpdb --dbuser=wpdbuser --dbpass=wpdbpass --dbhost=localhost --locale=fr_FR
```
- Installation de WordPress:
```bash
wp-cli core install --url='localhost/wp' --title='demo ansible' --admin_user='admin' --admin_password='password' --admin_email=admin@demo-ansible.fr --skip-email
```

>  Pour plus d'informations sur les commandes wp-cli : [https://developer.wordpress.org/cli/commands/](https://developer.wordpress.org/cli/commands/)



##  Phase : Ecrire des playbook

Ecrire des playbooks simples et efficaces pour réaliser les tâches ci-dessous. L'optimisation des playbooks se fera dans les tâches suivantes.

1. Installer les packages  "mysql-server", "apache2", "mysql", "php", "libapache2-mod-php", "php-mysql"
2. Démarrer les services "apache2" et "mysql-server"
3. Télecharger "WP-CLI", le renomer dans "/usr/local/bin/wp-cli", et le rendre exécutable (nécéssite des droits "root")
4. Créer le dossier "/var/www/html/wp", et modifier son propriétaire : user=user, group=user (nécéssite des droits "root")
5. Télécharger la dernière version de Wordpress en français,  dans le dossier "/var/www/html/wp"
6. Créer une base de données MySQL
7. Configurer l'accès à la base de données dans Wordpress avec la commabde wp-cli.
8. Configurer le compte d'admin dans Worpress avec la commabde wp-cli.

Modules à utiliser :

- apt : [https://docs.ansible.com/ansible/2.9/modules/apt_module.html](https://docs.ansible.com/ansible/2.9/modules/apt_module.html)
- service : [https://docs.ansible.com/ansible/2.9/modules/service_module.html](https://docs.ansible.com/ansible/2.9/modules/service_module.html)
- files : [https://docs.ansible.com/ansible/2.9/modules/file_module.html](https://docs.ansible.com/ansible/2.9/modules/file_module.html)
- get_url : [https://docs.ansible.com/ansible/2.9/modules/get_url_module.html](https://docs.ansible.com/ansible/2.9/modules/get_url_module.html)
- command : [https://docs.ansible.com/ansible/2.9/modules/command_module.html](https://docs.ansible.com/ansible/2.9/modules/command_module.html)
- mysql_db : [https://docs.ansible.com/ansible/2.9/modules/mysql_db_module.html](https://docs.ansible.com/ansible/2.9/modules/mysql_db_module.html)
- mysql_user : [https://docs.ansible.com/ansible/2.9/modules/mysql_user_module.html](https://docs.ansible.com/ansible/2.9/modules/mysql_user_module.html)



## Phase 2 : Utiliser des variables

| Variable | Default value |
| --------------- | --------------------- |
| wp_cli_path | /usr/local/bin |
| wp_cli_bin  | wp_cli_path + /wp-cli |
| wp_locale | fr_FR |
| wp_version | latest |
| wp_path | /var/www/html/wp |
| wp_dbname | wpdb |
| wp_dbuser | wpdbuser |
| wp_dbpass | wpdbpass |
| wp_dbhost | localhost |
| wp_site_url | localhost/wp |
| wp_title | demo ansible |
| wp_admin_user | admin |
| wp_admin_password | password |
| wp_admin_email | admin@demo-ansible.fr |
| wp_skip_email | true |


## Phase 3 : Utiliser des rôles

Utiliser des rôles pour installer LAMP. Par exemple :

- [https://galaxy.ansible.com/geerlingguy/apache](https://galaxy.ansible.com/geerlingguy/apache)
- [https://galaxy.ansible.com/geerlingguy/mysql](https://galaxy.ansible.com/geerlingguy/mysql)
- [https://galaxy.ansible.com/geerlingguy/php](https://galaxy.ansible.com/geerlingguy/php)


## Phase 4 : Améliorer l'idempotence

L'objectif de l'idempotence est de pouvoir relancer les tâches, et d'obtenir le même résultat que lors du premier lancement.

Gérer l'idempotence :

 - Télécharger Wordpress uniquement si le fichier '/var/www/html/wp/wp-settings.php' n'existe pas.
 - Test de la version courante par rapport à celle demandée ?
 - Configurer Wordpress uniquement si le fichier '/var/www/html/wp-config.php' n'existe pas.

Modules :

- stat
- register

## Phase 5 : créer un rôle Worpress


## Phase 6 : utiliser Ansible Vault pour améliorer la sécurité
