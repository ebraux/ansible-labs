# Gestion de l'inventory



## Préparation

Se déplacer dans le dossier du lab "inventory"
```bash
cd inventory
```

## Utilisation de l'inventory

La commande `ansible-inventory` permet d'afficher l'inventory comme il est vu par Ansible.


## Inventory basique

Le fichier 'inventory_basique' correspond à un inventory avec uniquement 2 Managed-Node.
```bash
cat inventory_basique
# srv-01
# srv-02
```

Affichage de son contenu avec la commande `ansible-inventory` :

```bash
ansible-inventory -i inventory_basique --yaml --list
```
``` yaml
all:
  children:
    ungrouped:
      hosts:
        srv-01: {}
        srv-02: {}
```

## Inventory complexe

Le fichier 'inventory_complexe' contient un inventory plus complexe, comprenant :

  - 2 groupes de serveurs, avec chacun 2 groupes enfants
  - 4 Managed-Nodes
  - Certains Managed-node appartiennent à plusieurs groupes

``` ini
[prod:children]
frontend_prod
backend_prod

[rec:children]
frontend_rec
backend_rec

[frontend_prod]
srv-front-01
srv-front-02

[backend_prod]
srv-back-01

[frontend_rec]
srv-rec-01

[backend_rec]
srv-rec-01
```

Pour afficher son contenu avec la commande `ansible-inventory` :
```bash
ansible-inventory -i inventory_complexe --yaml --list
```
``` yaml
all:
  children:
    prod:
      children:
        backend_prod:
          hosts:
            srv-back-01: {}
        frontend_prod:
          hosts:
            srv-front-01: {}
            srv-front-02: {}
    rec:
      children:
        backend_rec:
          hosts:
            srv-rec-01: {}
        frontend_rec:
          hosts:
            srv-rec-01: {}
    ungrouped: {}
```


## Configuration de Ansible dans l'inventory

Il est possible d'ajouter des informations de configuration spécifiques, ou des variables à chaque Managed-Node dans l'inventory :

```bash
srv01 ansible_port=2201 ansible_host=localhost
srv02  ansible_port=2202 ansible_host=localhost
```

## Utilisation du format YAML

L'inventory peut-être définit au format YAML

```yaml
all:
  children:
    prod:
      children:
        frontend-prod:
        backend-prod:

    rec:
      children:
        frontend-rec:
        backend-rec:

    frontend-prod:
      hosts:
        srv-front-01:
        srv-front-02:

    backend-prod:
      hosts:
        srv-back-01:

    frontend-rec:
      hosts:
        srv-rec-01:
          ansible_connection: ssh
          ansible_port: 22
          ansible_host: localhost
          ansible_user: root
          ansible_password: unsecure
          ansible_ssh_extra_args: '-o StrictHostKeyChecking=no'

```

Pour afficher son contenu avec la commande `ansible-inventory`
```bash
ansible-inventory -i inventory_complexe_yaml --yaml --list
```

## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---
## Informations complémentaires

* [https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

