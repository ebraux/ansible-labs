# Utilisation de template

## Préparation

Se déplacer dans le dossier du lab "templating"
```bash
cd templating
```

## Exemple de template

Utilisation d'un fichier de template pour créer un fichier "userlist.conf" à partir du contenu d'une liste (login,email), avec les contraintes :

- ne pas afficher l'utilisateur 'admin' : utilisation de la condition `if`
- afficher les emails en minuscule : utilisation du filtre `lower()`


Fichier de template : userlist.j2

```jinja2
{% for user in users %}
  {% if user.login != 'admin' %}
    login={{ user.login }}, email={{ user.email|lower() }}
  {% endif %}
{% endfor %}
```
Ci dessus, des indentations ont été ajoutée pour faciliter la compréhension. Mais dans fichier de template, on ne peut pas mettre les indentations, car ce sera des espaces dans le fichier de destination.

Le fichier doit être :
```jinja2
{% for user in users %}
{% if user.login != 'admin' %}
login={{ user.login }}, email={{ user.email|lower() }}
{% endif %}
{% endfor %}
```


Utilisation du module 'template"
```yaml
- name: Template a file to /etc/userlist.conf
  ansible.builtin.template:
    src: templates/userlist.j2
    dest: /etc/userlist.conf
    owner: root
    group: root
    mode: '0644'
```

Affichage du playbook, avec la liste des utilisateurs
```bash
cat create-userlist.yml
```

Affichage du template
```bash
cat templates/userlist.j2
```

Lancement de la création du fichier sur les Managed-Nodes
```bash
ansible-playbook create-userlist.yml
```

Vérification du résultat
```bash
ansible all -m ansible.builtin.shell -a 'cat /etc/userlist.conf'
# srv-02 | CHANGED | rc=0 >>
# login=user1, email=user1@acme.com
# login=User2, email=user2@acme.com
# srv-01 | CHANGED | rc=0 >>
# login=user1, email=user1@acme.com
# login=User2, email=user2@acme.com```
```

> Le test aurait pu être fait avec les commandes `ssh srv-01 cat /etc/userlist.conf` et `ssh srv-02 cat /etc/userlist.conf`.

 
---
## Fin du lab 

Revenir dans le dossier principal des labs
```bash
cd ..
```

---
## Informations Complémentaires

Quelques liens :

- Le module template : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)
- Jinja2 : [https://jinja.palletsprojects.com/en/2.11.x/templates/](https://jinja.palletsprojects.com/en/2.11.x/templates/)
- Les filtres de transformation de contenu : [https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters](https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters)
