
# Mise en place d'une connexion SSH securisée

## Configuration en place et bonnes pratiques

La configuration de l’environnement de Lab par défaut ne respecte pas les bonnes pratiques de sécurité des systèmes, qui recommandent de :

* ne pas utiliser directement un utilisateur "root", mais un autre utilisateur avec des droits 'sudo'
* ne pas se connecter en SSH en utilisant des mots de passe, mais une clé ssh
* ne pas permettre de se connecter en SSH en tant que root.


Pour appliquer les bonnes pratiques, il faut donc :

* créer un utilisateur "non root", avec droits "sudo", et connexion par clé SSH, et configurer Ansible pour qu'il utilise cet utilisateur.
* restreindre les droits des utilisateurs "root" et "user" existants.


## Liste des tâches à réaliser

Ce qui corresponds aux tâches suivantes :

1. créer une clé SSH
2. créer un utilisateur de management "ansible-control" sur les Managed-Nodes
3. activer une connexion sur les Managed-nodes par clé ssh pour l'utilisateur "ansible-control"
4. permettre à cet utilisateur "ansible-control" d'exécuter des commandes avec des droits root, via sudo sur les Managed-Nodes
5. tester le bon fonctionnement de l'utilisateur créé (connexion SSH et droits sudo)
6. configurer Ansible pour s'exécuter avec l'utilisateur "ansible-control"
7. supprimer l'utilisateur "user" sur les Managed-Nodes
8. interdire la connexion ssh avec mot de passe, et n'autoriser que l'utilisation de clés sur les Manged-Nodes.
9. interdire la connexion ssh à l'utilisateur root sur les Managed-Nodes

La plupart de ces tâches peuvent être effectuées avec Ansible.

Seules les tâches 1, 5 et 6 sont à faire sur le control-node, et seront faites manuellement.


## Éléments de configuration


### 1. Créer une clé SSH

Pour générer une clé SSH, utiliser la commande "ssh-keygen". Par exemple :
```bash
mkdir ssh-keys
ssh-keygen -t rsa -b 1024 -f ssh-keys/ansible-control -q -N ""
```
Cette commande génère un couple clé publique (ansible-control.pub) / clé privée (ansible-control), dans le dossier "keys".
```bash
ll ssh-keys
```

> La clé générée ici est une clé RSA de 1024bits. Une taille de 4096 bits est recommandée.


### 2. Créer un utilisateur de management "ansible-control" sur les Managed-Nodes

Pour gérer des groupes, on utilise le module "group" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html)

Pour gérer les utilisateurs, on utilise le module "user" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html)


### 3. Activer une connexion sur les Managed-nodes par clé ssh pour l'utilisateur "ansible-control"

Pour gérer l'authentification par clé SSH pour un utilisateur utiliser le module "authorized_key" : [https://docs.ansible.com/ansible/latest/collections/ansible/posix/authorized_key_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/posix/authorized_key_module.html)

!! BUG avec le paquet 'ansible_core' de la distribution Ubuntu22.04. La commande "ansible-galaxy" ne permet pas de manipuler les collections :
- [https://github.com/ansible/ansible/issues/77624](https://github.com/ansible/ansible/issues/77624)

La solution consiste à "downgrader" la librairie de la version 0.8.1, en 0.5.3 ...
``` bash
sudo apt install pip
pip install --force-reinstall -v resolvelib==0.5.3
```

Pour pouvoir ensuite installer la collection
``` bash
ansible-galaxy collection install ansible.posix 
```

Une fois la clé créée et déployée sur les Managed-Nodes, pour tester la connexion ssh, utiliser la commande
```bash
ssh -i ssh-keys/ansible-control ansible-control@srv-01
# ssh -i <Private_key_file> <user>@<host>
```

### 4. Permettre à l'utilisateur "ansible-control" d’exécuter des commandes avec des droits root, via sudo sur les Managed-Nodes

Pour donner des droits "root" à un utilisateur on utilise le mécanisme "sudo".

La configuration se fait en créant le fichier '/etc/sudoers.d/ansible-control', avec le contenu
```
%ansible-control ALL=(ALL) NOPASSWD: ALL
```

Pour créer un fichier, on utilise le module [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)


Pour la gestion des fichiers de configuration, les bonnes pratiques recommandent d'utiliser plutôt des fichiers de configuration de référence, et le module "template" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)


### 5. Tester le bon fonctionnement de l'utilisateur créé (connexion SSH et droits sudo)

Se connecter à un Managed-Node en ssh, avec la clé SSH générée
```bash
ssh -i ssh-keys/ansible-control ansible-control@srv-01
# ssh -i <Privte_key_file> <user>@<host>
```

Une fois connecté au Managed-node, se connecter en tant que root :
```bash
ansible-control@srv-01$ sudo -i
# le shell devient root@@srv-01#
```

###  6. configurer ansible pour s'exécuter avec l'utilisateur "ansible-control"

La connexion SSH utilisée par Ansible peut être configurée dans le fichier "ansible.cfg" en utilisant :

- la variable "remote_user" pour définir l'utilisateur à utiliser
- la variable "private_key_file" pour. définir la clé privée à utiliser

> Il est également possible d'utiliser les option "--remote_user" "--private-key" en ligne de commande.

Pour tester la connection ansible, utilise la commande linux `whoami`, avec le module shell [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html).


### 7. Supprimer l'utilisateur "user" sur les Managed-Nodes

Pour gérer les utilisateurs, on utilise le module "user" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html)

Pour gérer des groupes, on utilise le module "group" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html)


### 8. Interdire la connexion ssh avec mot de passe, et n'autoriser que l'utilisation de clés sur les Managed-Nodes.

Pour modifier les paramètres de connexion SSH, il faut modifier la configuration du serveur openssh-server. Le fichier de configuration est /etc/ssh/sshd_config.

Il faut configurer la variable `PasswordAuthentication no`.

Pour que cette modification soit prise en compte, il faut ensuite demander `sshd` de recharger sa configuration. Pour le lab s'exécutant dans un environnement Docker, arrêter Openssh reviendrait à tuer le conteneur. Il faut donc utiliser l'action `reload`, et non pas `restart`



Pour modifier le contenu d'un fichier, on peut utiliser le module "lineinfile" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html)

Pour la gestion des fichiers de configuration, les bonnes pratiques recommandent d'utiliser plutôt des fichiers de configuration de référence, et le module "template" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)

Pour gérer des service (redémarrage,  ...)  on utilise le module "service" : [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html)

Pour la gestion des services, les bonnes pratiques recommandent d'utiliser le mécanisme de handler : [https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_handlers.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_handlers.html)


### 9. Interdire la connexion ssh à l'utilisateur root sur les Managed-Nodes

Le mode opératoire est le même que pour la tâche précédente.

Il faut configurer la variable `PermitRootLogin no`.


## Amélioration

Quelques pistes d'amélioration

- utiliser un rôle pour gérer la mise en place du sudo
- utiliser un rôle pour gérer la configuration de openssh-server


## Informations Complémentaires

Quelques liens :

- [https://stackoverflow.com/questions/25629933/ansible-copy-ssh-key-from-one-host-to-another](https://stackoverflow.com/questions/25629933/ansible-copy-ssh-key-from-one-host-to-another)
- [https://www.howtoforge.com/tutorial/setup-new-user-and-ssh-key-authentication-using-ansible/](https://www.howtoforge.com/tutorial/setup-new-user-and-ssh-key-authentication-using-ansible/)
- [https://www.ssh.com/ssh/keygen/](https://www.ssh.com/ssh/keygen/)
