# Environnement pour les labs "ansible-lab"

[https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)


---
## Principe

Installation de l'environnement du lab "Ansible"

- Mise à jour du système, et Installation des dépendances et prérequis
- Configuration de l'accès ssh par mot de passe
- Installation de Docker
- Récupération et préparation du lab

---
## Préparation

Installation d'Ansible, via python-pip. 
- Utilisation de direnv recommandée [https://direnv.net/docs/installation.html](https://direnv.net/docs/installation.html)
- A défaut, utiliser 
``` bash 
# Ansible
pip install -r pip-requirements-ansible.txt

# Ansible roles and Collections 
ansible-galaxy install -r requirements.yml
```

vérifier la configuration d'Ansible

``` bash
which ansible
```

``` bash
ansible --version
# ansible [core 2.12.0]
# ...
```

---
## Configuration

 
- Liste des noeuds du lab : `inventory/hosts`, au format `lab01 ansible_host=xxx.xxx.xxx.xxx`

- Ajuster les informations de connexion aux noeuds:
  - user : dans le fichier `user_login.txt`
  - password : dans le fichier `user_password.txt`


---
## Utilisation

``` bash
ansible-playbook deploy.yml  
```

- Si besoin, configurer  le `remote_user`  dans le fichier `ansible.cfg`

- Ou utiliser le fichier user_login.txt : 
``` bash
ansible-playbook deploy.yml --user `cat user_login.txt` 
```

