---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 80%](img/ansible.png)



## Ansible Labs 

### Environnement pour les Labs Ansible

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)



---
![bg right:60% 90%](img/ansible-archi_simple.jpg)
# Principe

**Composition du LAB :**

- un Control-Node, 
- 2 Managed-Nodes.


---
# Environnement

- Sur le **Control-Node**, sont installés :
  - Ansible-core
  - Divers outils permettant d'interagir avec les Managed-Nodes (ping, curl ...)
  - Divers outils permettant de travailler sur les labs (nano, ...)
- Sur les **Managed-Node**
  - Seul openssh-server est installé pour permettre les connexions par SSH.
 
> Les Managed-Nodes seront configurés via Ansible à mesure du déroulement des Labs.

---
# Utilisation du LAB

## Objectif : se concentrer sur Ansible

- **Gestion du LAB automatisée** : initialisation, start, stop, tests
- **Ansible pré-installé**
- **Gestion des connexions SSH simplifée** 


---
# Connexions SSH

- Configuration SSH simplifiée, et non-secure
- Utilisateurs "root" et "user" du Controle-Node peuvent se connecter en SSH en tant que "root"sur les Managed-Nodes :
    - soit en utilisant un mot de passe,
    - soit sans mot de passe, en utilisant une clé publique.
- Le mot de passe à utiliser dans toutes les situations est : unsecure.

### C'est une configuration en mode LAB. Ce n'est pas une bonne pratique !


---
# Gestion du LAB

**Utilisation de la commande `make`**
* Se placer dans le dossier d'environnement :
  * par exemple : `cd docker`
* Initialiser l'environnement : `make create`
* Lancer l'environnement des labs :  `make start`
* Tester le bon fonctionnement de l'environnement : `make test`
* Accéder au Control-Node pour dérouler les labs : `make go`

---
# Sous le capot de l’environnement Docker

- Utilisation "détournée" de Docker 
  - Plus souple, et moins gourmand en ressource que des VMs
- Création d'une image de base commune
  - A partir d'une image Ubuntu 22.04
  - Installation d'open-ssh
  - Configuration des clés SSH communes
  - Autorisation des accès "root", login par mot de passe et par SSH Keypair.
- Pour les Managed-Node, 
  - Lancement d'open-ssh, et d'un shell pour maintenir les containers "actifs".

---
# Initialisation du LAB

Récupérer l'environnement de Labs depuis le dépôt :
``` bash 
git clone https://gitlab.com/ebraux/ansible-labs.git
```
Initialiser le lab
``` bash
cd ansible-labs/docker
make create
```
Lancer le LAB, et se connecter au Control-node Ansible
``` bash
make start
make go
```
---
# C'est parti

- Vous êtes connectés :
  - sur le Control-node "controller"
  - en tant qu'utilisateur "user" .
- Votre shell doit être: `user@controler:/labs$`


### Vous pouvez commencer les labs : [https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)




