
# Labs de découverte de Ansible.

---

La version en ligne de ce dépôt est disponible sur le site : [https://ebraux.gitlab.io/ansible-labs](https://ebraux.gitlab.io/ansible-labs)

---

- Accéder à la documentation des labs :  [labs](labs)
- Informations pour développement du site : [development.md](development.md)

