FROM python:3.9-bullseye

LABEL maintainer="Emmnanuel Braux <emmanuel.braux@imt-atlantique.fr>"

USER root

COPY requirements.txt  /srv/requirements.txt

RUN pip install --no-cache-dir -r /srv/requirements.txt

WORKDIR /work

# changement d'utilisateur pour sécuriser
USER $NB_UID
